import { hello } from "../src";
test('hello("hello") returns 5', function () {
    expect(hello("hello")).toEqual(5);
});
