module.exports = {
  rootDir: '.',
  cacheDirectory: '<rootDir>/.cache/unit',
  preset: 'ts-jest',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  modulePathIgnorePatterns: ["<rootDir>/dist/"],
  testEnvironment: 'node',
  collectCoverageFrom: [
    '<rootDir>/src/*.ts',
    '!<rootDir>/test/**'
  ],
  collectCoverage: true,
  coverageReporters: ['json', 'html']
}
