import { hello } from "../src";

test('hello("hello") returns "hellohellohellohellohello"', () => {
  expect(hello("hello")).toEqual("hellohellohellohellohello")
})
